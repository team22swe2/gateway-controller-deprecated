# Gateway Controller

## Endpoints
### PUT controller/heartbeats/GWId
+ receives heartbeat from Diagnostic (for SaaS MVP) / Device Simulator (for Post Saas MVP)
+ calls putHeartbeat() with data received
+ returns 500 as error code

***

## Primary Functions
## putHeartbeat(GWId)
+ Recurring diagnostic heartbeat indication to CIC, default one hearbeat/minute frequency
+ Heartbeat request shall include GW ID, location latitude/longitude and local timestamp.
+ Heartbeat will include recurring status information such as CPU temperature and battery power.
+ CIC REST response will specify any on-demand diagnostic tests or scheduling to be run in the gateway  

## putResult(GWId, timestamp, testType, testResult) 
+ might run after a response from CIC for test results
+ sends results after test runs  

## getDaily(GWId)
+ get copy of latest daily diagnostics requests  

***

## Helper Functions

### sendPost(body, url)
+ sends POST request to CIC, used for reccuring heardbeat & sending test results

### readFile()
+ used for SaaS MVP
+ reads local file for heartbeat information

### handleSchedules(GWId, method: string (e.g. add, edit, delete))
+ potentially called after a "piggyback" response from putHeartbeat()
+ will send a request to CIC to make record of the new schedule

### handleTests(GWId, testType)
+ potentially called after a "piggyback" response from putHeartbeat()
+ communicates with GW Diagnostic to run the testType received
