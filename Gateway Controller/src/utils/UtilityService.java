package utils;

import java.io.BufferedReader;
import org.json.JSONObject;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

public class UtilityService {
	
	// Makes POST request and print response
	public static JSONObject executePost(String targetURL, String urlParameters, JSONObject body) {
		  HttpURLConnection connection = null;
		  JSONObject jsonRes;

		  try {
		    //Create connection
			 
		    URL url = new URL(targetURL);
		    connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("POST");
		    connection.setRequestProperty("Content-Type", 
		        "application/x-www-form-urlencoded");
		    connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		    connection.setRequestProperty("Accept", "application/json");
//		    connection.setRequestProperty("Content-Length", 
//		        Integer.toString(urlParameters.getBytes().length));
//		    connection.getOutputStream().write(query.getBytes("UTF8"));
		    connection.setRequestProperty("Content-Language", "en-US");  

		    connection.setUseCaches(false);
		    connection.setDoOutput(true);
		    connection.setDoInput(true);

		    
		    
		    //Send request
		    OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());
		    wr.write(body.toString());
		    
//		    DataOutputStream wr = new DataOutputStream (
//		        connection.getOutputStream());
//		    wr.writeBytes(urlParameters);
//		    wr.close();

		    //Get Response  
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
		    String line;
		    while ((line = rd.readLine()) != null) {
		      response.append(line);
		      response.append('\r');
		    }
		    rd.close();
		    
		    jsonRes = new JSONObject(response.toString());
		    
		    return jsonRes;
		  } catch (Exception e) {
		    e.printStackTrace();
		    return null;
		  } finally {
		    if (connection != null) {
		      connection.disconnect();
		    }
		  }
		}
}
