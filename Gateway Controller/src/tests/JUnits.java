package tests;

import utils.UtilityService;
import static org.junit.jupiter.api.Assertions.*;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

class JUnits {

	@Test
	void test_executePOST() {
		JSONObject response = utils.UtilityService.executePost("https://jsonplaceholder.typicode.com/posts", "", new JSONObject());
		assertEquals(response.get("id"), 101);
	}
	
	@Test
	void test_executePOST_CIC() {
		JSONObject response = utils.UtilityService.executePost("https://team22.softwareengineeringii.com/cic/heartbeat", "", new JSONObject());
		assertTrue(response.keySet().contains("heartbeat"));
	}
}
