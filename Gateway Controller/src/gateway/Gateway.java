package gateway;

import java.util.Date;

public interface Gateway {
	
	public final static int MAXIMUM_THREADS = 8;
	public final static int GATEWAY_ID = 3;
	
	// sends POST to CIC with heartbeat information
	// CIC response will indicate other method calls like handleSchedule or handleTests
	public void putHeartbeat(int gwID);
	
	// get copy of latest daily diagnostics requests
	public void getDaily(int gwID);
	
	// upon completion of a diagnostic test, test results shall be reported to CIC.
	public void putResult(int gwID, Date timestamp, String testType, String testResult);
}
