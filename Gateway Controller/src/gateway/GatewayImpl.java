package gateway;

import java.util.Date;

import org.json.JSONObject;

public class GatewayImpl implements Gateway {

	@Override
	public void putHeartbeat(int gwID) {
		
		JSONObject parent = new JSONObject();
		JSONObject data = new JSONObject();
		
		data.put("timestamp", "10/11/2019");
		data.put("turnSpeed", 45);
		
		parent.put("heartbeat", data);
		
		JSONObject postResponse = utils.UtilityService.executePost("https://team22.softwareengineeringii.com/cic/heartbeat", "", parent);
		System.out.println(postResponse);
	}

	@Override
	public void getDaily(int gwID) {
		// TODO Auto-generated method stub
	}

	@Override
	public void putResult(int gwID, Date timestamp, String testType, String testResult) {
		// TODO Auto-generated method stub
	}
}
