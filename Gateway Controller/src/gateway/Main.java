package gateway;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;



public class Main {
	
	public static void main (String args[]) throws IOException {
		String fromClient;
        String toClient;
 
        ServerSocket server = new ServerSocket(8080);
        System.out.println("wait for connection on port 8080");
 
        boolean run = true;
        while(run) {
            Socket client = server.accept();
            System.out.println("got connection on port 8080");
            
            // in listens for heartbeat
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            
            // out sends back to diagnostic
            PrintWriter out = new PrintWriter(client.getOutputStream(),true);
 
            // reads data that's been received
            fromClient = in.readLine();
            System.out.println("received: " + fromClient);
            
            JSONObject json = new JSONObject();
            
            // attempts to convert string to JSON
            try {
            	json = new JSONObject(fromClient);
            } catch (JSONException err) {
            	System.out.println("Error Caught");
            }
 
            if(!json.isEmpty()) {
            	
                toClient = "test";
                out.println(toClient);
                
                // makes cic request
                GatewayImpl instance = new GatewayImpl();
                instance.putHeartbeat(instance.GATEWAY_ID);
                
                client.close();
                System.out.println("Client Closed");
            }
        }
        System.exit(0);
	}
}
